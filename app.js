const steem = require("steem");
const yaml = require('js-yaml');
const fs = require('fs');
const log = require('simple-node-logger').createSimpleLogger('app.log');
const express = require('express');
const expressApp = express();
const mysql = require('mysql');

//config
try {
    var config = yaml.safeLoad(fs.readFileSync(__dirname + '/config.yml', 'utf8'));
}
catch (e) {
    log.info(e);
}

var con = mysql.createConnection({
    host: config['mysql']['host'],
    user: config['mysql']['user'],
    password: config['mysql']['password'],
    database: config['mysql']['database']
});

/*
STEEM NETWORK THINGS
*/
if (config['steem']['testnet']) {
    //curl --data "username=artur9010&password=passw0rd" https://testnet.steem.vc/create
    steem.api.setOptions({url: 'wss://testnet.steem.vc'});
    steem.config.set('address_prefix', 'STX');
    steem.config.set('chain_id', '79276aea5d4877d9a25892eaa01b0adf019d3e5cb12a97478df3298ccdd01673')
} else {
    //steem.api.setOptions({ url: 'wss://node.steem.ws' })
}

/*
{ from: 'artur9010',
to: 'steemcraft.com',
amount: '0.001 SBD',
memo: 'uuid' }
*/

steem.api.streamOperations(function (err, operations) {
    operations.forEach(function (operation) {
        if (operation["to"] == config['steem']['username']) {
            if (operation["memo"]) { //transaction contains memo
                log.info(operation);
                if (operation["amount"].includes("SBD")) {
                    var memo = operation["memo"];
                    if (memo.length == 36) { //probably uuid
                        log.info(operation["amount"]);
                        var msbd = operation["amount"].substring(0, operation["amount"].length - 3);
                        //log.info(msbd);
                        msbd = Number(msbd);
                        //log.info(msbd);
                        msbd = msbd * 1000; //convert SBD to mSBD
                        log.info(msbd);
                        con.connect(function (err) {
                            if (err) throw err;
                            var sql = "UPDATE players SET coins = coins+" + msbd + " WHERE uuid='" + memo + "'";
                            con.query(sql, function (err, result) {
                                if (err) throw err;
                            });
                        });
                    }
                }
            }
        }
    });
});
/*
WEB APP THINGS
*/
expressApp.get('/withdraw/:secret/:username/:amount', function (req, res) {
    if (req.params.secret == config['api']['secret']) {
        var sbd = (req.params.amount / 1000).toFixed(3);
        steem.broadcast.transfer(config['steem']['activeKey'], config['steem']['username'], req.params.username, sbd + ' SBD', 'SteemCraft.com Minecraft Server withdraw. Thanks for playing! http://steemcraft.com/', function (err, result) {
            log.info(err, result);
            if (!err) {
                res.send("ok");
            } else {
                res.send("error");
            }
        });
    } else {
        res.send("invalid_secret");
    }
});

expressApp.get('/toSavings/:secret/:amount', function (req, res) {
    if (req.params.secret == config['api']['secret']) {
        var sbd = (req.params.amount / 1000).toFixed(3);
        steem.broadcast.transferToSavings(config['steem']['activeKey'], config['steem']['username'], config['steem']['username'], sbd + ' SBD', 'SteemCraft.com (fee or something else)', function (err, result) {
            log.info(err, result);
            if (!err) {
                res.send("ok");
            } else {
                res.send("error");
            }
        });
    } else {
        res.send("invalid_secret");
    }
});

expressApp.get('/balance', function (req, res) {
    steem.api.getAccounts(["steemcraft.com"], function (err, result) {
        if (!err) {
            var balance = result[0]['sbd_balance']
            balance = balance.substring(0, balance.length - 4);
            balance = parseFloat(balance) * 1000;
            res.send(balance.toString());
        }
    });
});

expressApp.listen(process.env.PORT || 3000);
